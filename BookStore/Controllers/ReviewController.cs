﻿using BookStore.Models;
using System.Linq;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class ReviewController : BaseController
    { 
        public ActionResult Create(int bookId, string name)
        {
            var review = new BookReview() { BookId = bookId, Name = name };
            return View(review);
        }

        [HttpPost]
        public ActionResult Create(BookReview newReview)
        {
            if (ModelState.IsValid)
            {
                var errors = ModelState.SelectMany(x => x.Value.Errors.Select(z => z.Exception));
                _context.AddReview(newReview);
                return RedirectToAction("DetailsBook", "Home", new { id = newReview.BookId });
            }
            else
            {
                ModelState.AddModelError("Create", "Something wrong!");
            }
            return View(newReview);
        }

        [HttpPost]
        public void AddReport(int reviewId, string reason)
        {
            var review = _context.GetReview(reviewId);
            if (review == null)
            {
                Response.StatusCode = 404;
                return;
            }

            review.IsOffensive = true;
            review.ReportReason = reason;
            _context.Update(review); 
        }

        [HttpPost]
        public int AddLike(int reviewId)
        {
            var review = _context.GetReview(reviewId);
            if (review == null)
            {
                Response.StatusCode = 404;
                return -1;
            }

            ++review.Likes;
            _context.Update(review);

            return review.Likes;
        }
    }
}

