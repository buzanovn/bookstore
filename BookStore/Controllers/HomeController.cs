﻿using BookStore.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class HomeController : BaseController
    {
        private int unitsInPage = 10;
        public ActionResult Contact()
        {
            ViewBag.Message = "Контактные данные";

            return View();
        }

        public ActionResult Users()
        {
            ViewBag.Message = "Пользователи";
            var users = _context.GetReviews();
            return View(users);
        }

        public ActionResult CreateBook()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateBook(Book book)
        {
            if (ModelState.IsValid)
            {
                _context.AddBook(book);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("CreateBook", "Something wrong!");
            }
            return View(book);
        }

        public ActionResult DetailsBook(int Id = 1)
        {
            var book = _context.GetBook(Id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        public ActionResult EditBook(int Id)
        {
            var book = _context.GetBook(Id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        [HttpPost]
        public ActionResult EditBook(Book book)
        {
            if (ModelState.IsValid)
            {
                _context.Update(book);
                return RedirectToAction("DetailsBook", "Home", new { id = book.Id });
            }
            else
            {
                ModelState.AddModelError("EditBook", "Something wrong!");
            }
            return View(book);
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Index(int? id)
        {
            int page = id ?? 0;
            if (Request.IsAjaxRequest())
            {
                return PartialView("_Items", GetItemsPage(page));
            }
            return View(GetItemsPage(page));
        }

        private List<Book> GetItemsPage(int page = 1)
        {
            var itemsToSkip = page * unitsInPage;

            return _context.GetBooks().OrderBy(t => t.Id).Skip(itemsToSkip).
                Take(unitsInPage).ToList();
        }
    }
}