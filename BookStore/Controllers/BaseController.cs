﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class BaseController : Controller
    {
        public IBookContext _context;

        public BaseController() : this(ContextFactory.getContext())
        {
        }

        public BaseController(IBookContext bookContext)
        {
            _context = bookContext ?? throw new ArgumentNullException();
        }
    }
}