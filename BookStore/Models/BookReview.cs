﻿using LinqToDB.Mapping;
using System;
using System.ComponentModel.DataAnnotations;

namespace BookStore.Models
{
    [Table(Name="BookReviews")]
    public class BookReview
    {
        [PrimaryKey, Identity]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Имя"), Column]
        public String Name { get; set; }

        [Required]
        [Display(Name = "Комментарий"), Column]
        public String Review { get; set; }

        [Column]
        public int BookId { get; set; }

        [Column]
        public int Likes { get; set; }

        [Column]
        public bool IsOffensive { get; set; }

        [Display(Name = "Жалоба"), Column]
        public string ReportReason { get; set; }

        public void Update(BookReview other)
        {
            this.Name = other.Name;
            this.Review = other.Review;
            this.Likes = other.Likes;
            this.IsOffensive = other.IsOffensive;
            this.ReportReason = other.ReportReason;
        }
    }
}