﻿using BookStore.Models.Context;

namespace BookStore.Models
{
    public static class ContextFactory
    {
        enum Type { MOCK, DATABASE };

        /// <summary>
        /// Change BuildType to anything like Release to use real Database
        /// </summary>
        /// <returns></returns>
        private static Type getContextType()
        {
#if DEBUG
            return Type.MOCK;
#else
            return Type.DATABASE;
#endif
        }

        public static IBookContext getContext()
        {
            Type contextType = getContextType();
            if (contextType == Type.MOCK)
            {
                return new MockContext();
            }
            else
            {
                return new DatabaseContext();
            }
        }
    }
}