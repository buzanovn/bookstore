﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Models.Context
{
    public class MockContext : IBookContext
    {
        private T wrapMockDatabaseAction<T>(Func<MockDatabase, T> func) 
            => func.Invoke(MockDatabase.Instance);
        private void wrapMockDatabaseAction(Action<MockDatabase> func) 
            => func.Invoke(MockDatabase.Instance);
        public void AddBook(Book newBook) => wrapMockDatabaseAction(db 
            => db.Books.Add(newBook));
        public void AddReview(BookReview newReview) => wrapMockDatabaseAction(db 
            => db.Reviews.Add(newReview));

        public void Dispose() { }

        public Book GetBook(int bookId) 
            => wrapMockDatabaseAction(db => db.Books.FirstOrDefault(x => x.Id == bookId));

        public List<Book> GetBooks() 
            => wrapMockDatabaseAction(db => db.Books.ToList());

        public BookReview GetReview(int id) 
            => wrapMockDatabaseAction(db => db.Reviews.FirstOrDefault(x => x.Id == id));

        public List<BookReview> GetReviews() 
            => wrapMockDatabaseAction(db => db.Reviews.ToList());

        public List<BookReview> GetReviews(int bookId) 
            => wrapMockDatabaseAction(db => db.Reviews.Where(x => x.BookId == bookId).ToList());

        public Book Update(Book newBookData)
        {
            var oldBook = GetBook(newBookData.Id);
            wrapMockDatabaseAction(db => db.Books.FirstOrDefault(x => x.Id == newBookData.Id).Update(newBookData));
            oldBook.Update(newBookData);
            return oldBook;
        }

        public BookReview Update(BookReview newBookReview)
        {
            var oldReview = GetReview(newBookReview.Id);
            wrapMockDatabaseAction(db => db.Reviews.FirstOrDefault(x => x.Id == newBookReview.Id).Update(newBookReview));
            oldReview.Update(newBookReview);
            return oldReview;
        }
    }

    class MockDatabase
    {
        private static MockDatabase instance;

        private MockDatabase() {
            Books = new List<Book>();
            Reviews = new List<BookReview>();
            for (int i = 0; i < 150; i++)
            {
                Books.Add(new Book
                {
                    Id = i,
                    Name = "Test" + i,
                    Author = "Author" + i,
                    Genre = "Genre" + i
                });

                Reviews.Add(new BookReview
                {
                    Id = i,
                    BookId = i,
                    Name = "Name" + i,
                    Review = "R!!!" + i
                });
            }
        }

        public static MockDatabase Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MockDatabase();
                }
                return instance;
            }
        }

        public List<Book> Books { get; }

        public List<BookReview> Reviews { get; }
    }
}