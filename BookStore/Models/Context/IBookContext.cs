﻿using System;
using System.Collections.Generic;

namespace BookStore.Models
{
    public interface IBookContext : IDisposable
    {
        void AddBook(Book newBook);
        void AddReview(BookReview newReview);
        List<Book> GetBooks();
        List<BookReview> GetReviews();
        List<BookReview> GetReviews(int bookId);
        BookReview GetReview(int id);
        Book GetBook(int bookId);
        Book Update(Book newBookData);
        BookReview Update(BookReview newBookReview);
    }
}