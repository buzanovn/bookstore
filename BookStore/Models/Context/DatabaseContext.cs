﻿using BookStore.Models;
using LinqToDB;
using LinqToDB.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookStore.Models
{
    public class DatabaseContext : IBookContext
    {
        private T wrapDatabaseAction<T>(Func<Database, T> func)
        {
            using (var db = new Database())
            {
                return func.Invoke(db);
            }
        }
        public void AddBook(Book newBook) => wrapDatabaseAction(db => db.Insert(newBook));

        public void AddReview(BookReview newReview) => wrapDatabaseAction(db => db.Insert(newReview));

        public Book GetBook(int bookId) => wrapDatabaseAction(db => db.Books.First(x => x.Id == bookId));

        public Book Update(Book newBookData)
        {
            var oldBook = GetBook(newBookData.Id);
            wrapDatabaseAction(db => db.Update(newBookData));
            oldBook.Update(newBookData);
            return oldBook;
        }

        public List<Book> GetBooks() 
            => wrapDatabaseAction(db => db.Books.ToList());

        public List<BookReview> GetReviews() 
            => wrapDatabaseAction(db => db.Reviews.ToList());

        public List<BookReview> GetReviews(int bookId) 
            => wrapDatabaseAction(db => db.Reviews.Where(x => x.BookId == bookId).ToList());


        public void Dispose() { }

        public BookReview GetReview(int id) 
            => wrapDatabaseAction(db => db.Reviews.FirstOrDefault(x => x.Id == id));

        public BookReview Update(BookReview newBookReview)
        {
            var oldReview = GetReview(newBookReview.Id);
            wrapDatabaseAction(db => db.Update(newBookReview));
            oldReview.Update(newBookReview);
            return oldReview;
        }
    }

    class Database : DataConnection
    {
        public Database() : base("Main") { }

        public ITable<Book> Books { get { return GetTable<Book>(); } }

        public ITable<BookReview> Reviews { get { return GetTable<BookReview>(); } } 
    }
}